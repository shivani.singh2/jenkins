
provider "aws" {
  region = "us-east-1"
  profile = var.profile
}

data "aws_vpc" "name" {
  cidr_block = "172.31.0.0/16"
}

output "vpc" {
  value = data.aws_vpc.name.id
}
 
variable "profile" {
    default = ""
}
